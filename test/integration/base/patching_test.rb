# # encoding: utf-8

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe file('/hab/svc/config-baseline/data/cache/cache/chef_patching_sentinel') do
  it { should exist }
end


describe file('/etc/passwd') do
  it { should exist }
  it { should be_file }
  it { should be_owned_by 'root' }
  its('group') { should eq 'root' }
  it { should_not be_executable }
  it { should be_writable.by('owner') }
  it { should_not be_writable.by('group') }
  it { should_not be_writable.by('other') }
  it { should be_readable.by('owner') }
  it { should be_readable.by('group') }
  it { should be_readable.by('other') }
end