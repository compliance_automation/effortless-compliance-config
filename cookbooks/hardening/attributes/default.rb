default['hardening']['waiver_local_location'] = "/tmp"
default['hardening']['waiver_source_location'] = "https://gitlab.com/compliance_automation/waivers/-/raw/master"
default['hardening']['waiver_env'] = "prod"
default['hardening']['audit_package'] = "compliance-audit"