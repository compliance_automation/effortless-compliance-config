require "net/http"

def url_check?(target)
    url = URI.parse(target)
    req = Net::HTTP.new(url.host, url.port)
    req.use_ssl = true
    res = req.request_head(url.path)
    res.code != "404"
rescue Errno::ENOENT
    false
end

res_master = url_check?("#{node['hardening']['waiver_source_location']}/master.toml")

remote_file "#{node['hardening']['waiver_local_location']}/master.toml" do
    source "#{node['hardening']['waiver_source_location']}/master.toml"
    # owner 'hab'
    # group 'hab'
    mode '0755'
    use_conditional_get true
    action :create
    only_if { res_master }
end

res1 = url_check?("#{node['hardening']['waiver_source_location']}/Environments/#{node['hardening']['waiver_env']}.toml")

remote_file "#{node['hardening']['waiver_local_location']}/env.toml" do
    source "#{node['hardening']['waiver_source_location']}/Environments/#{node['hardening']['waiver_env']}.toml"
    mode '0755'
    use_conditional_get true
    action :create
    only_if { res1 }
end


res = url_check?("#{node['hardening']['waiver_source_location']}/Nodes/#{node['hostname']}.toml")

remote_file "#{node['hardening']['waiver_local_location']}/node.toml" do
    source "#{node['hardening']['waiver_source_location']}/Nodes/#{node['hostname']}.toml"
    mode '0755'
    use_conditional_get true
    action :create
    only_if { res }
end

template "/tmp/waiver_config.toml" do
    source "waiver_config.toml.erb"
end

bash "append_master_waivers" do
    code <<-EOF
        echo "\n" > /tmp/waiver_config.toml
        cat /tmp/master.toml >> tmp/waiver_config.toml
    EOF
    only_if { ::File.exists?('/tmp/master.toml') }
end

bash "append_env_waivers" do
    code <<-EOF
        echo "\n" >> /tmp/waiver_config.toml
        cat /tmp/env.toml >> /tmp/waiver_config.toml
    EOF
    only_if { ::File.exists?('/tmp/env.toml') }
end

bash "append_node_waivers" do
    code <<-EOF
        echo "\n" >> /tmp/waiver_config.toml
        cat /tmp/node.toml >> /tmp/waiver_config.toml
    EOF
    only_if { ::File.exists?('/tmp/node.toml') }
end

bash "apply_hab_config" do
    code <<-EOF
        hab config apply #{node['hardening']['audit_package']}.default $(date +%s) /tmp/waiver_config.toml
    EOF
end