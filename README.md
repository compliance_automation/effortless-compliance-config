# Compliance Automation Effortless Package

This repository contains the reference architecture for the Effortless Config pattern with the addition of a recipe that manages waiver files for use in demoing waviers in Effortless Audit. For full documentation of the Effortless Config pattern, please refer to the examples in the Chef Effortless repository (<https://github.com/chef/effortless/tree/master/examples/effortless_config)>

The README will focus on the specifics of the waiver.rb recipe

## waiver.rb

The waiver functionality has been added as a recipe in this repository under cookbooks/hardening/recipes

### Purpose

The purpose of the waiver.rb recipe is to enable defining a set of InSpec waivers to be distributed to systems that are running Effortless Audit in conjunction with Effortless Config. The cookbook allows for "master" waivers that apply to all managed systems, "Environment" waivers that apply only to systems configured with a particular environment, and "Node" waivers that apply only to specific individual systems by hostname (as reported by `node['hostname']` attriubte)

### Setup and Operation

#### Cookbook Attributes

The following cookbook attributes are used by the waiver.rb recipe.

- `node['hardening']['waiver_source_location']`: remote URL location of waiver files
- `node['hardening']['waiver_local_locaiton']`: local location on the node to save downloaded waiver files
- `node['hardening']['waiver_env']`: used to identify which environment waiver file to use for a node
- `node['hardening']['audit_package']`: name of the Effortless Audit service running on the node
- `node['hostname']`: this is the standard ohai hostname attribute, used to match node to Nodes waiver file

#### Waiver Files

Waiver files must exist somewhere reachable by the target systems via URL. They must be toml files, as they will be applied as habitat configs to the Effortless Audit service. Here is the format for a waiver entry in a toml file:

```
[waivers.control01]
run = false
justification = "control exception approved by Joe Security"
expiration_date = "2028-05-08"
```

The url for these files is set by the cookbook attribute `node['hardening']['waiver_source_location']`.

The cookbook expects there to be a file `https://#{node['hardening']['waiver_source_location']}/master.toml`.

Optionally, there can be files for Environments at `https://#{node['hardening']['waiver_source_location']}/Environments/#{node['hardening']['waiver_env']}.toml`

as well as `https://#{node['hardening']['waiver_source_location']}/Nodes/#{node['hostname']}.toml`

This GitLab repo is an example of this structure: <https://gitlab.com/compliance_automation/waivers>. To use this repo, the value of `node['hardening']['waiver_source_location']` would be `https://gitlab.com/compliance_automation/waivers/-/tree/master`.

#### Operation

The waivers.rb recipe operates as follows:

1. Validates that the master.toml address is valid. If it is, downloads master.toml
2. Checks whether the Environments/#{node['hardening']['waiver_env']}.toml address is valid. If it is, downloads that file
3. Checks whether the Nodes/#{node['hostname']}.toml address is valid. If it is, downloads that file
4. Concatonates any and all downloaded files into a single file
5. Executes `hab config apply` against the defined Effortless Audit service name with the concatonated file.
